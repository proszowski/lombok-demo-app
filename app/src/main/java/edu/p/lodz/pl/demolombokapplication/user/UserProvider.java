package edu.p.lodz.pl.demolombokapplication.user;


import java.util.Collection;
import java.util.function.Consumer;

public interface UserProvider {
    void subscribeForUsers(Consumer<Collection<User>> consumer);
}
