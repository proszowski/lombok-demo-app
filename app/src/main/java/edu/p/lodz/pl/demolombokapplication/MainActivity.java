package edu.p.lodz.pl.demolombokapplication;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import edu.p.lodz.pl.demolombokapplication.post.MockPostProvider;
import edu.p.lodz.pl.demolombokapplication.post.Post;
import edu.p.lodz.pl.demolombokapplication.post.PostProvider;
import edu.p.lodz.pl.demolombokapplication.user.MockUserProvider;
import edu.p.lodz.pl.demolombokapplication.user.User;
import edu.p.lodz.pl.demolombokapplication.user.UserProvider;

public class MainActivity extends AppCompatActivity {

    private UserProvider userProvider;
    private PostProvider postProvider;
    private Map<String, User> users = new HashMap<>();
    private Collection<Post> posts = new ArrayList<>();
    private Spinner spinner;
    private TableLayout tableLayout;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spinner);
        tableLayout = findViewById(R.id.table);
        userProvider = new MockUserProvider(this);
        userProvider.subscribeForUsers(this::updateUsers);
        postProvider = new MockPostProvider(this);
        postProvider.subscribeForPosts(this::updatePosts);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                String userName = textView.getText().toString();
                displayPostsOfUserWithName(userName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updatePosts(Collection<Post> posts) {
        this.posts = posts;
        String userName = (String) spinner.getSelectedItem();
        displayPostsOfUserWithName(userName);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void displayPostsOfUserWithName(String userName) {
        User user = users.get(userName);
        tableLayout.removeAllViews();
        if(user == null){
            return;
        }
        posts
             .stream()
             .filter(post -> post.getUserId().equals(user.getId()))
             .forEach(post -> {
                    TableRow separatorRow = new TableRow(this);

                    TextView separator = new TextView(this);
                    separator.setText(String.format("\n%s\n", post.getTitle()));
                    separator.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                    separatorRow.addView(separator);

                    TableRow bodyRow = new TableRow(this);
                    TextView body = new TextView(this);
                    body.setText(post.getBody());
                    body.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                    bodyRow.addView(body);

                    tableLayout.addView(separatorRow);
                    tableLayout.addView(bodyRow);
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateUsers(Collection<User> users) {
        users.forEach(user -> this.users.put(user.getName(), user));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                users.stream().map(User::getName).collect(Collectors.toList()));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
}
