package edu.p.lodz.pl.demolombokapplication.post;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public class MockPostProvider implements PostProvider{

    private Collection<Post> posts = new ArrayList<>();

    private List<Consumer<Collection<Post>>> subscribers = new ArrayList<>();

    private static String URL = "https://jsonplaceholder.typicode.com/posts";

    @RequiresApi(api = Build.VERSION_CODES.N)
    public MockPostProvider(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        Request request = new StringRequest(Request.Method.GET,
                URL,
                response -> {
                    posts = Arrays.asList(new Gson().fromJson(response, Post[].class));
                    subscribers.forEach( subscriber -> subscriber.accept(posts) );
                },
                System.out::println);
        queue.add(request);
    }

    @Override
    public void subscribeForPosts(Consumer<Collection<Post>> consumer) {
        subscribers.add(consumer);
    }
}
