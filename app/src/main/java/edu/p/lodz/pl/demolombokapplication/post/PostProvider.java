package edu.p.lodz.pl.demolombokapplication.post;

import java.util.Collection;
import java.util.function.Consumer;

public interface PostProvider {
    void subscribeForPosts(Consumer<Collection<Post>> consumer);
}
