package edu.p.lodz.pl.demolombokapplication.user;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public class MockUserProvider implements UserProvider {

    private static String URL = "https://jsonplaceholder.typicode.com/users";
    private Collection<User> users = new ArrayList<>();
    private List<Consumer<Collection<User>>> subscribers = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.N)
    public MockUserProvider(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        Request request = new StringRequest(Request.Method.GET,
                URL,
                response -> {
                    users = Arrays.asList(new Gson().fromJson(response, User[].class));
                    subscribers.forEach( subscriber -> subscriber.accept(users) );
                },
                System.out::println);
        queue.add(request);
    }

    @Override
    public void subscribeForUsers(Consumer<Collection<User>> consumer) {
        subscribers.add(consumer);
    }
}
